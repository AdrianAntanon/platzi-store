export const environment = {
  production: true,
  url_api: 'https://platzi-store.herokuapp.com',
  firebase: {
    apiKey: 'AIzaSyAffbVkvkoXPhxatVYUJTbnUaJk7UEyuRU',
    authDomain: 'platzi-store-afd31.firebaseapp.com',
    databaseURL: 'https://platzi-store-afd31.firebaseio.com',
    projectId: 'platzi-store-afd31',
    storageBucket: 'platzi-store-afd31.appspot.com',
    messagingSenderId: '683141450861',
    appId: '1:683141450861:web:02915fb28841a1e08f8364',
  },
};
